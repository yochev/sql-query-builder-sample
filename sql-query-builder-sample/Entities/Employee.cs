﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sql_query_builder_sample.Entities
{
    class Employee : BaseEntity
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
