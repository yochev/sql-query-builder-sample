﻿using System;
using System.Collections.Generic;
using System.Text;

namespace sql_query_builder_sample.Entities
{
    class BaseEntity : IEntity
    {
        public int Id { get; set; }
    }
}
