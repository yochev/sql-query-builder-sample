﻿using sql_query_builder_sample.Entities;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq.Expressions;
using System.Text;

namespace sql_query_builder_sample.Builders
{
    class MsSqlQueryBuilder<T> : IQueryBuilder<T> where T : IEntity
    {
        public string BuildSelect(string tableName)
        {
            return $"SELECT * FROM {tableName}";
        }

        private List<string> GetPropertiesNames(T entity)
        {
            List<string> names = new List<string>();
            foreach (PropertyInfo pi in entity.GetType().GetProperties())
            {
                names.Add(pi.Name);
            }

            return names;
        }

        private string GetColumnsList(List<string> propertiesNames)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in propertiesNames)
            {
                sb.Append(item)
                    .Append(", ");
            }

            return sb.ToString().Substring(0, sb.Length - 2);
        }

        private string GetParametersList(List<string> propertiesNames)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in propertiesNames)
            {
                sb.Append("@")
                    .Append(item)
                    .Append(", ");
            }

            return sb.ToString().Substring(0, sb.Length - 2);
        }

        public string BuildInsert(string tableName, T entity)
        {
            List<string> propertiesNames = GetPropertiesNames(entity);
            return $"INSERT INTO {tableName} {GetColumnsList(propertiesNames)} VALUES {GetParametersList(propertiesNames)}";
        }
        
        public string BuildUpdate(string tableName, T entity)
        {
            throw new NotImplementedException();
        }

        public string BuildDelete(string tableName, T entity)
        {
            throw new NotImplementedException();
        }
    }
}
