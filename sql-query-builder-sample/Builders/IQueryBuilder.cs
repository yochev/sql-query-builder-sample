﻿using sql_query_builder_sample.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace sql_query_builder_sample.Builders
{
    interface IQueryBuilder<T> where T : IEntity
    {
        string BuildSelect(string tableName);
        string BuildInsert(string tableName, T entity);
        string BuildUpdate(string tableName, T entity);
        string BuildDelete(string tableName, T entity);
    }
}
