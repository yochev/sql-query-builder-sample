﻿using sql_query_builder_sample.Builders;
using sql_query_builder_sample.Entities;
using System;

namespace sql_query_builder_sample
{
    class Program
    {
        static void Main(string[] args)
        {
            IQueryBuilder<Employee> builder = new MsSqlQueryBuilder<Employee>();
            string tableName = "Employees";
            Employee employee = new Employee();

            Console.WriteLine(builder.BuildSelect(tableName));
            Console.WriteLine(builder.BuildInsert(tableName, employee));
        }
    }
}
